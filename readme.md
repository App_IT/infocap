## Info Capture Web App

This application was designed to easily capture contact information for people interested in the App Academy during events which may not have internet access.

## Installation

This is a "standalone" version of the Info Capture App, but the Info Capture App can easily be run from a centralized webserver.

For example, a webserver + infoCap could be attached to a wireless router for larger events where data aggregation would be overly tedious.

##### Using Windows / XAMPP

1. Ensure XAMPP is installed
2. Delete the contents of the folder C:\XAMPP\htdocs
3. Copy he contents of the directory where this readme file resides to C:\XAMPP\htdocs
4. Using the XAMPP Control Panel, start Apache and MySQL
5. Go to 127.0.0.1/phpmyadmin and log in
6. Create a database in phpmyadmin called "appsignup"
7. Import C:\XAMPP\htdocs\db\appsignup.sql into the "appsignup" database
8. Restart Apache and MySQL. This may not be necessary, but is good practice

That's all.  Once the database has been imported and Apache and MySQL are running, you should be able to get the Info Capture Web App by pointing any browser to `http://localhost` or `http://127.0.0.1`.

You should run the browser(s) in "Incognito Mode" or similar to prevent form fill and autocomplete from leaking information entered by your newly acquired contacts.

### Using (Ubuntu) Linux / Apache
(Instructions Under Constructions)

1. Install Ubuntu-server with LAMP
2. Choose "root" for MySQL password, or note the MySQL root password you created
3. Install phpmyadmin
    * `$ sudo apt-get install phpmyadmin`
4. Clone infoCap into /var/www/html
    * `$ git clone https://bitbucket.org/App_IT/infoCap`
5. Go to 127.0.0.1/phpmyadmin and log in as root
6. Create a database in phpmyadmin called "appsignup"
7. Import C:\XAMPP\htdocs\db\appsignup.sql into the "appsignup" database
8. Restart Apache and MySQL. This may not be necessary, but is good practice
    * `$ sudo service apache2 restart && sudo service mysql restart`

That's all.  Once the database has been imported and Apache and MySQL are running, you should be able to get the Info Capture Web App by pointing any browser to `http://localhost/infoCap` or `http://127.0.0.1/infoCap`.

You should run the browser(s) in "Incognito Mode" or similar to prevent form fill and autocomplete from leaking information entered by your newly acquired contacts.

You can test that input is working check on the current state of the database by pointing the browser to:
`http://localhost/infoCap/db`

When finished, use phpmyadmin to export the data to CSV or your favorite file format.

## License
Open Source / MIT License