-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2014 at 03:37 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `appsignup`
--

-- --------------------------------------------------------

--
-- Table structure for table `octoberevent`
--

CREATE TABLE IF NOT EXISTS `octoberevent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentFirstName` varchar(64) NOT NULL,
  `studentLastName` varchar(64) NOT NULL,
  `currentSchool` varchar(64) NOT NULL,
  `studentCellPhone` varchar(64) NOT NULL,
  `studentEmail` varchar(64) NOT NULL,
  `parentFirstName` varchar(64) NOT NULL,
  `parentLastName` varchar(64) NOT NULL,
  `parentCellPhone` varchar(64) NOT NULL,
  `parentEmail` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
